import time
import os
import fileinput


# получает данные
def get_data():
    data = []  # кадры считанные из файла
    data_item = ''
    for line in fileinput.input():
        if line.startswith("#") or line.startswith("\n"):
            continue
        if line.startswith("```") and len(data_item) == 0:
            continue
        if line.startswith("```") and len(data_item) > 0:
            data.append(data_item)
            data_item = ''
            continue
        else:
            data_item += line
    return data


# рисует кадры
def draw_data(data):
    # очищаем экран
    os.system('cls')
    i = 0
    while i < len(data):
        # '\033[H' - перемещает курсор в начало экрана
        # '\033[94m' - устанавливает цвет курсора
        print('\033[H' + '\033[94m' + data[i])
        # задержка
        time.sleep(0.1)

        if i == len(data) - 1:
            # возвращаемся на первый кадр
            i = 0
        else:
            # берем следующий кадр
            i += 1


if __name__ == '__main__':
    try:
        draw_data(get_data())
    except KeyboardInterrupt:
        # сбрасывает цвет курсора
        print('\033[0m' + 'end')
